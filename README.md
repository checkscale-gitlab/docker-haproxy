10 ez steps for the sweet, sweet LOCK 

1. git clone https://gitlab.com/MrMikeandike/docker-haproxy.git

2. move your cert into haproxy folder (can use the name mai.me.cert.pem if you dont want to change the configs)

3. run haproxy/create_network.sh

4. change haproxy.cfg to use the new cert name (replace mai.me.cert.pem with whatever your cert is called)

5. on line 31 of haproxy.cfg -- change the pattern (default is MAIportainer(\.mikeandike\.me)?, the question mark around the () makes it not care if domain name is there. just set it to application.yourdomain.yourdomain_end)

6. change Dockerfile to copy from your cert name rather than mai.me.cert.pem

7. while in haproxy dir, run "sudo docker build -t mai/haproxy:1.0" ."

8. while in haproxy dir, run "sudo docker-compose up -d" and make sure the container is up with "sudo docker-compose ps"

9. change to portainer dir

10. "sudo docker-compose up -d" and make sure its up with "sudo docker-compose ps"


any request on port 80 with the headername as whatever you set the pattern to will now route to the portainer container with ssl.
on windows, you can add the url to your host file and set the ip to the server. I added MAIportainer.mikeandike.me as the IP of my VM and it works.

